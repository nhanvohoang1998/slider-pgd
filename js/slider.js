var counter = 0;
divs = $('[data-delay]');
console.log(divs)

$(document).ready(function () {
    showDiv();
});

function showDiv() {
    divs.hide();
    let cur = divs.eq(counter);
    cur.fadeIn(300, "linear");
    counter++;
    if (counter == divs.length) {
        counter = 0; //Reset Counter
    }
    setTimeout(showDiv, cur.data('delay')*1000);
};